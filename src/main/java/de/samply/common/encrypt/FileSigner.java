/*
 * Copyright (c) 2017. Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.common.encrypt;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;

/**
 * Class to create signatures of files and verify them
 */
public class FileSigner {
    /**
     * Creates a signature for a file and returns it as byte array
     * 
     * @param privateKey
     * @param fileToEncrypt
     * @return signature as bytearray
     * @throws InvalidKeyException
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws SignatureException
     */
    public byte[] doSign(PrivateKey privateKey, File fileToEncrypt)
            throws InvalidKeyException, NoSuchAlgorithmException, IOException, SignatureException {
        return doSign(privateKey, fileToEncrypt, null);
    }

    /**
     * Creates a signature for a file, returns it as byte array and also writes
     * it into the given file
     * 
     * @param privateKey
     * @param fileToEncrypt
     * @param fileToWriteSignatureIn
     * @return signature as bytearray
     * @throws InvalidKeyException
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws SignatureException
     */
    public byte[] doSign(PrivateKey privateKey, File fileToEncrypt, File fileToWriteSignatureIn)
            throws InvalidKeyException, NoSuchAlgorithmException, IOException, SignatureException {
        Signature rsaSigner = Signature.getInstance("SHA1withRSA");
        rsaSigner.initSign(privateKey);

        try (FileInputStream fis = new FileInputStream(fileToEncrypt);
                BufferedInputStream bis = new BufferedInputStream(fis);) {
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = bis.read(buffer)) >= 0) {
                rsaSigner.update(buffer, 0, len);
            }
        }

        byte[] signature = rsaSigner.sign();

        if (fileToWriteSignatureIn != null) {
            try (FileOutputStream sigfos = new FileOutputStream(fileToWriteSignatureIn);) {
                sigfos.write(signature);
            }
        }

        return signature;
    }

    /**
     * Verifies the signature of a file
     * 
     * @param publicKey
     * @param signature
     * @param signedFile
     * @return
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public boolean verifySign(PublicKey publicKey, File signature, File signedFile) throws IOException,
            NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {

        try (FileInputStream sigfis = new FileInputStream(signature);) {
            byte[] sigToVerify = new byte[sigfis.available()];
            sigfis.read(sigToVerify);
            return verifySign(publicKey, sigToVerify, signedFile);
        }
    }

    public boolean verifySign(PublicKey publicKey, InputStream signature, InputStream signedFile) throws IOException,
            NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
        byte[] sigToVerify = new byte[signature.available()];
        signature.read(sigToVerify);
        return verifySign(publicKey, sigToVerify, signedFile);
    }

    public boolean verifySign(PublicKey publicKey, byte[] signatureToVerifyWith, InputStream signedStream)
            throws IOException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException,
            SignatureException {
        Signature sig = Signature.getInstance("SHA1withRSA");
        sig.initVerify(publicKey);

        try (BufferedInputStream bufin = new BufferedInputStream(signedStream);) {

            byte[] buffer = new byte[1024];
            int len;
            while (bufin.available() != 0) {
                len = bufin.read(buffer);
                sig.update(buffer, 0, len);
            }
            boolean verifies = sig.verify(signatureToVerifyWith);
            return verifies;
        }
    }

    /**
     * Verifies the signature of a file
     * 
     * @param publicKey
     * @param signatureToVerifyWith
     * @param signedFile
     * @return
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public boolean verifySign(PublicKey publicKey, byte[] signatureToVerifyWith, File signedFile) throws IOException,
            NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
        Signature sig = Signature.getInstance("SHA1withRSA");
        sig.initVerify(publicKey);

        try (FileInputStream datafis = new FileInputStream(signedFile);
                BufferedInputStream bufin = new BufferedInputStream(datafis);) {

            byte[] buffer = new byte[1024];
            int len;
            while (bufin.available() != 0) {
                len = bufin.read(buffer);
                sig.update(buffer, 0, len);
            }
        }

        boolean verifies = sig.verify(signatureToVerifyWith);
        return verifies;
    }
}
