/*
 * Copyright (c) 2017. Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.common.encrypt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class PublicAndPrivateKeyMaker {

    /**
     * gets java.security.PrivateKey by private key filename
     * 
     * @param privateKeyFileName
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static PrivateKey getPrivateKey(String privateKeyFileName)
            throws NoSuchAlgorithmException, InvalidKeySpecException, FileNotFoundException, IOException {
        File privateKeyFile = new File(privateKeyFileName);

        return getPrivateKey(privateKeyFile);
    }

    /**
     * gets java.security.PrivateKey by private key file
     * 
     * @param privateKeyFile
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static PrivateKey getPrivateKey(File privateKeyFile)
            throws NoSuchAlgorithmException, InvalidKeySpecException, FileNotFoundException, IOException {
        byte[] encodedKey = new byte[(int) privateKeyFile.length()];
        try (FileInputStream fis = new FileInputStream(privateKeyFile)) {
            fis.read(encodedKey);
        }
        PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedKey);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = kf.generatePrivate(privateKeySpec);

        return privateKey;
    }

    /**
     * gets java.security.PublicKey by public key file name
     * 
     * @param publicKeyFileName
     * @return
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static PublicKey getPublicKey(String publicKeyFileName)
            throws InvalidKeySpecException, NoSuchAlgorithmException, FileNotFoundException, IOException {
        File publicKeyFile = new File(publicKeyFileName);
        return getPublicKey(publicKeyFile);
    }

    /**
     * gets java.security.PublicKey by public key file
     * 
     * @param publicKeyFile
     * @return
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static PublicKey getPublicKey(File publicKeyFile)
            throws InvalidKeySpecException, NoSuchAlgorithmException, FileNotFoundException, IOException {
        byte[] encodedKey = new byte[(int) publicKeyFile.length()];
        try (FileInputStream fis = new FileInputStream(publicKeyFile)) {
            fis.read(encodedKey);
        }

        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encodedKey);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PublicKey publicKey = kf.generatePublic(publicKeySpec);

        return publicKey;
    }
}
