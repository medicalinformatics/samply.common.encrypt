/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.common.encrypt;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * Utility class for encrypting/decrypting files.
 * 
 * Encryption workflow: 1) Creates a new AES key 2) Encrypts the files with the
 * AES key 3) Encrypts the AES key with a given public RSA key (DER format)
 *
 * Decryption workflow: 1) Decrypts the AES key with a given private RSA key
 * (DER format) 2) Decrypts the files with the AES key
 */
public class FileEncryption {
    /**
     * AES key size set to 128 (due to Java restrictions by US export laws) If
     * you want to use a bigger key, you will need to install Sun's Unlimited
     * Strength Jurisdiction Policy Files Leave this at 128, if you want bigger,
     * set it with the setAesKeySize method or use the Constructor
     */
    public static final int AES_Key_Size = 128;

    /**
     * Cipers
     */
    private Cipher pkCipher, aesCipher;

    /**
     * the AES key as Bytearray
     */
    private byte[] aesKey;

    /**
     * AES key Spec
     */
    private SecretKeySpec aeskeySpec;

    /**
     * Custom AES key size
     */
    private int aesKeySize;

    /**
     * Constructor: Creates ciphers
     * 
     * @throws GeneralSecurityException
     */
    public FileEncryption() throws GeneralSecurityException {
        // standard setting
        aesKeySize = AES_Key_Size;
        // create RSA public key cipher
        pkCipher = Cipher.getInstance("RSA");
        // create AES shared key cipher
        aesCipher = Cipher.getInstance("AES");
    }

    /**
     * Constructor: Creates ciphers
     * 
     * @param aesKeySize
     *            custom keysize (minimum is 128)
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     */
    public FileEncryption(int aesKeySize) throws GeneralSecurityException {
        if (aesKeySize < 128)
            aesKeySize = 128;

        this.aesKeySize = aesKeySize;
        // create RSA public key cipher
        pkCipher = Cipher.getInstance("RSA");
        // create AES shared key cipher
        aesCipher = Cipher.getInstance("AES");
    }

    /**
     * Creates a new AES key
     * 
     * @throws NoSuchAlgorithmException
     */
    public void makeKey() throws NoSuchAlgorithmException {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(aesKeySize);
        SecretKey key = kgen.generateKey();
        aesKey = key.getEncoded();
        aeskeySpec = new SecretKeySpec(aesKey, "AES");
    }

    /**
     * Decrypts an AES key from a file using an RSA private key
     * 
     * @param in
     *            the File from where to read the encrypted AES key
     * @param privateKey
     *            the private key to decrypt with
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public void loadKey(File in, PrivateKey privateKey) throws GeneralSecurityException, IOException {
        pkCipher.init(Cipher.DECRYPT_MODE, privateKey);
        aesKey = new byte[aesKeySize / 8];
        try (CipherInputStream is = new CipherInputStream(new FileInputStream(in), pkCipher)) {
            is.read(aesKey);
        }
        aeskeySpec = new SecretKeySpec(aesKey, "AES");
    }

    /**
     * Decrypts an AES key from a file using an RSA private key
     * 
     * @param in
     *            the File from where to read the encrypted AES key
     * @param privateKeyFile
     *            the File with the private key to decrypt with (DER format)
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public void loadKey(File in, File privateKeyFile) throws GeneralSecurityException, IOException {
        PrivateKey privateKey = PublicAndPrivateKeyMaker.getPrivateKey(privateKeyFile);
        loadKey(in, privateKey);
    }

    /**
     * Decrypts an AES key from a string using an RSA private key
     * 
     * @param aesKeyEncryptedBase64encoded
     *            the encrypted AESkey as String (Base64 encoded, handles both
     *            URLsafe and not)
     * @param privateKey
     *            the private key to decrypt with
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public void loadKey(String aesKeyEncryptedBase64encoded, PrivateKey privateKey)
            throws GeneralSecurityException, IOException {
        pkCipher.init(Cipher.DECRYPT_MODE, privateKey);
        aesKey = new byte[aesKeySize / 8];

        byte[] base64decodedAESKey = Base64.decodeBase64(aesKeyEncryptedBase64encoded);
        InputStream in = new ByteArrayInputStream(base64decodedAESKey);

        try (CipherInputStream is = new CipherInputStream(in, pkCipher)) {
            is.read(aesKey);
        }
        aeskeySpec = new SecretKeySpec(aesKey, "AES");
    }

    /**
     * Decrypts an AES key from a string using an RSA private key
     * 
     * @param aesKeyEncryptedBase64encoded
     *            the encrypted AESkey as String (Base64 encoded, handles both
     *            URLsafe and not)
     * @param privateKeyFile
     *            the File with the private key to decrypt with (DER format)
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public void loadKey(String aesKeyEncryptedBase64encoded, File privateKeyFile)
            throws GeneralSecurityException, IOException {
        PrivateKey privateKey = PublicAndPrivateKeyMaker.getPrivateKey(privateKeyFile);
        loadKey(aesKeyEncryptedBase64encoded, privateKey);
    }

    /**
     * Encrypts the AES key to a file using an RSA public key
     * 
     * @param out
     *            the File to which to store the encrypted AES key
     * @param publicKey
     *            the public key to encrypt with
     * @throws IOException
     * @throws GeneralSecurityException
     */
    public void saveKey(File out, PublicKey publicKey) throws IOException, GeneralSecurityException {
        pkCipher.init(Cipher.ENCRYPT_MODE, publicKey);
        CipherOutputStream os = new CipherOutputStream(new FileOutputStream(out), pkCipher);
        os.write(aesKey);
        os.close();
    }

    /**
     * Encrypts the AES key to a file using an RSA public key
     * 
     * @param out
     *            the File to which to store the encrypted AES key
     * @param publicKeyFile
     *            the File with the public key to encrypt with (DER format)
     * @throws IOException
     * @throws GeneralSecurityException
     */
    public void saveKey(File out, File publicKeyFile) throws IOException, GeneralSecurityException {
        PublicKey publicKey = PublicAndPrivateKeyMaker.getPublicKey(publicKeyFile);
        saveKey(out, publicKey);
    }

    /**
     * Encrypts the AES key using an RSA public key and returns it as Base64
     * URLsafe encoded String
     * 
     * @param publicKey
     *            the public key to encrypt with
     * @return the encrypted AESkey as Base64 URLsafe encoded String
     * @throws IOException
     * @throws GeneralSecurityException
     */
    public String saveKey(PublicKey pk) throws IOException, GeneralSecurityException {
        pkCipher.init(Cipher.ENCRYPT_MODE, pk);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        CipherOutputStream os = new CipherOutputStream(baos, pkCipher);
        os.write(aesKey);
        os.close();

        return Base64.encodeBase64URLSafeString(baos.toByteArray());
    }

    /**
     * Encrypts the AES key using an RSA public key and returns it as Base64
     * URLsafe encoded String
     * 
     * @param publicKeyFile
     *            the File with the public key to encrypt with (DER format)
     * @return the encrypted AESkey as Base64 URLsafe encoded String
     * @throws IOException
     * @throws GeneralSecurityException
     */
    public String saveKey(File publicKeyFile) throws IOException, GeneralSecurityException {
        PublicKey publicKey = PublicAndPrivateKeyMaker.getPublicKey(publicKeyFile);
        return saveKey(publicKey);
    }
    
    /**
     * Encrypts and then copies the contents of a given file.
     * 
     * @param in
     *            the file to encrypt
     * @param out
     *            the encrypted file to save to
     * @throws IOException
     * @throws InvalidKeyException
     */
    public void encrypt(File in, File out) throws IOException, InvalidKeyException {
        aesCipher.init(Cipher.ENCRYPT_MODE, aeskeySpec);

        try (FileInputStream is = new FileInputStream(in);
                CipherOutputStream os = new CipherOutputStream(new FileOutputStream(out), aesCipher);) {
            copy(is, os);
        }
    }

    /**
     * Decrypts and then copies the contents of a given file.
     * 
     * @param in
     *            The file to decrypt
     * @param out
     *            The file to save to
     * @throws IOException
     * @throws InvalidKeyException
     */
    public void decrypt(File in, File out) throws IOException, InvalidKeyException {
        aesCipher.init(Cipher.DECRYPT_MODE, aeskeySpec);

        try (CipherInputStream is = new CipherInputStream(new FileInputStream(in), aesCipher);
                FileOutputStream os = new FileOutputStream(out);) {
            copy(is, os);
        }
    }
    
    /**
     * Decrypts and then copies the contents of a given file.
     * 
     * @param in
     *            The file to decrypt
     * @param out
     *            The file to save to
     * @throws IOException
     * @throws InvalidKeyException
     */
    public void decrypt(InputStream in, OutputStream out) throws IOException, InvalidKeyException {
        aesCipher.init(Cipher.DECRYPT_MODE, aeskeySpec);

        try (CipherInputStream is = new CipherInputStream(in, aesCipher)) {
            copy(is, out);
        }
    }

    /**
     * Copies a stream.
     * 
     * @param is
     * @param os
     * @throws IOException
     */
    private void copy(InputStream is, OutputStream os) throws IOException {
        int i;
        byte[] b = new byte[1024];
        while ((i = is.read(b)) != -1) {
            os.write(b, 0, i);
        }
    }

    /**
     * Encrypts a given string with the AESkey and returns it as Base64URL
     * encoded String. Basic idea is to use this to encrypt the filename, but of
     * course you can use this for any other strings, too :P
     * 
     * @param clearString
     * @return encryptedAndBase64EncodedString
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws InvalidKeyException
     */
    public String encryptFileName(String clearString)
            throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        aesCipher.init(Cipher.ENCRYPT_MODE, aeskeySpec);
        String encryptedAndBase64EncodedString = Base64
                .encodeBase64URLSafeString(aesCipher.doFinal(clearString.getBytes()));

        return encryptedAndBase64EncodedString;
    }

    /**
     * @see encryptFileName
     */
    public String encryptString(String clearString)
            throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        return encryptFileName(clearString);
    }

    /**
     * Decrypts a given encrypted and Base64URL encoded string
     * 
     * @param encryptedAndBase64URLEncodedString
     * @return clearString
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public String decryptFileName(String encryptedAndBase64URLEncodedString)
            throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        aesCipher.init(Cipher.DECRYPT_MODE, aeskeySpec);
        byte[] clearString = aesCipher.doFinal(Base64.decodeBase64(encryptedAndBase64URLEncodedString));

        return new String(clearString);
    }

    /**
     * @see decryptFileName
     */
    public String decryptString(String encryptedAndBase64EncodedString)
            throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        return decryptFileName(encryptedAndBase64EncodedString);
    }

    public int getAesKeySize() {
        return aesKeySize;
    }

    public void setAesKeySize(int aesKeySize) {
        this.aesKeySize = aesKeySize;
    }
}