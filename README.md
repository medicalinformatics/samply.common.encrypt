# Samply Encrypt Project

## General information

LIB to encrypt and decrypt files. 
Keys are encrypted/decrypted with a shared key, which is encrypted/decrypted by a public key algorithm.

Do note, that due to US export laws, Sun's Java is limited to 128bit keys. You will need to install Sun's Unlimited Strength Jurisdiction Policy Files if you want to use bigger keys.

## Preparation
Build your own RSA keys with openssl, we need them in PKCS#8, DER format: 

<code>

openssl genrsa -out private.pem 2048  
openssl pkcs8 -topk8 -in private.pem -outform DER -out private.der -nocrypt  
openssl rsa -in private.pem -pubout -outform DER -out public.der  
</code>

## Usage

### File encryption and decryption
Option 1: With storing the AES key into a file, you have to provide the file representation

<code>
FileEncryption secure = new FileEncryption();

// to encrypt a file  
secure.makeKey();  
secure.saveKey(encryptedAESKeyFile, publicKeyFile);  
secure.encrypt(fileToEncrypt, encryptedFile);  

// to decrypt it again  
secure.loadKey(encryptedAESKeyFile, privateKeyFile);  
secure.decrypt(encryptedFile, unencryptedFile);  
</code>

Option 2: AES key as Base64 encoded String

<code>
// to encrypt a file  
secure.makeKey();  
String AESkey = secure.saveKey(publicKeyFile);  
secure.encrypt(fileToEncrypt, encryptedFile);  

// to decrypt it again  
secure.loadKey(AESkey, privateKeyFile);  
secure.decrypt(encryptedFile, unencryptedFile);  
</code>

### Encryption of a filename (or string)
You can also encrypt a string with the shared AES key.
Basic idea is to encrypt the filename this way, but you can of course encrypt any other string, too :)

<code>

FileEncryption secure = new FileEncryption();  
secure.makeKey();  
String encryptedHallo = secure.encryptFileName(clearHallo);   
String decryptedHallo = secure.decryptFileName(encryptedHallo)   
</code>

### Signature and Verification
To create the signature of a file, you need to provide the file. It will give you the signature as byte array.
You can also opt to have it write the signature into a file by providing either a filename or a file representation to write the signature to 

<code>

// create the signature  
File signatureFile = new File("/tmp/shodan.sig");  
byte[] signature = fs.doSign(privateKey, theFile, signatureFile);  
</code>

To verify, you need to provide the signature (either as file or as byte array), the public key, and the file which was signed

<code>

// verify the signature  
PublicKey publicKey = PublicAndPrivateKeyMaker.getPublicKey(publicKeyFile);  
boolean ok = fs.verifySign(publicKey, signature, theFile);  
</code>

## Build

Use maven to build the jar:

```
mvn clean install
```
